#include "methods.h"

Methods::Methods()
{

}

void Methods::initQPainter(QPainter *painter, int widthWindow, int heightWindow) {
    this->painter = painter;
    this->widthWindow = widthWindow;
    this->heightWindow = heightWindow;
}

void Methods::myPaintCircle (double x0, double y0, int radius) {
    x0 = widthWindow /2 + x0;
    y0 = heightWindow /2 - y0;
    int x = 0;
    int y = radius;
    int delta = 1 - 2 * radius;
    int error = 0;
    while(y >= 0) {
        painter->drawPoint( (x0 + x), (y0 + y) );
        painter->drawPoint( (x0 + x), (y0 - y) );
        painter->drawPoint( (x0 - x), (y0 + y) );
        painter->drawPoint( (x0 - x), (y0 - y) );
        error = 2 * (delta + y) - 1;
        if(delta < 0 && error <= 0) {
            ++x;
            delta += 2 * x + 1;
            continue;
        }
        error = 2 * (delta - x) - 1;
        if(delta > 0 && error > 0) {
            --y;
            delta += 1 - 2 * y;
            continue;
        }
        ++x;
        delta += 2 * (x - y);
        --y;
    }
}

void Methods::myPaintCircle (Point *point, int radius) {
    myPaintCircle(point->sx, point->sy, radius);
}

void Methods::MyPaintLine(int x1, int y1, int x2, int y2)
{
    int deltaX = abs(x2 - x1);
    int deltaY = abs(y2 - y1);

    int signX;
    if(x1 < x2) {
        signX = 1;
    } else {
        signX = -1;
    }
    int signY;
    if(y1 < y2) {
        signY = 1;
    } else {
        signY = -1;
    }
    int error = deltaX - deltaY;

    painter->drawPoint(widthWindow/2 + x2, heightWindow/2 - y2);
    while(x1 != x2 || y1 != y2)
    {

         painter->drawPoint(widthWindow/2 + x1, heightWindow/2 - y1);
        const int error2 = error * 2;
        //
        if(error2 > -deltaY)
        {
            error -= deltaY;
            x1 += signX;
        }
        if(error2 < deltaX)
        {
            error += deltaX;
            y1 += signY;
        }
    }
}
void Methods::MyPaintLine(Point *point1, Point *point2)
{
    MyPaintLine(point1->sx, point1->sy, point2->sx, point2->sy);
}
void Methods::myPaintPoint(int x, int y) {
    //Задать цвет перед Point
    QPen penHLines(QColor(255,255,255), 2.5, Qt::DotLine, Qt::FlatCap, Qt::RoundJoin);
    painter->drawPoint( widthWindow /2 + x, heightWindow /2 - y);
}
void Methods::myPaintPoint(Point *point, QColor color) {

    Methods::myPaintPoint(point, color, 2.5);

}
void Methods::myPaintPoint(Point *point, QColor color, double size) {
    double x = widthWindow /2 + point->sx;
    double y = heightWindow /2 - point->sy;
    //painter->setBrush(QColor(255,0,0));
    QPen penHLines(color, size, Qt::DotLine, Qt::FlatCap, Qt::RoundJoin);
    painter->setPen(penHLines);
    painter->drawPoint( x, y);
}
void Methods::myPaintPoint(Point *point) {
    double x = widthWindow /2 + point->sx;
    double y = heightWindow /2 - point->sy;
    QPen penHLines(QColor(255,255,255), 2.5, Qt::DotLine, Qt::FlatCap, Qt::RoundJoin);
    painter->drawPoint( x, y);
}
