#ifndef POINT_H
#define POINT_H

#include <iostream>
#include "math.h"

using namespace std;

class Point
{
public:
    int x;
    int y;
    int z;

    int sx;
    int sy;
    int sz;

    int colorR = 255;
    int colorG = 255;
    int colorB = 255;

    double vToLight[3];

    void calcVectorToLight(int normal[3] ,int xLight, int yLight, int zLight);

    void setX(int x);
    void setY(int y);
    void setZ(int z);

    void setSX(int sx);
    void setSY(int sy);
    void setSZ(int sz);

    Point();
    Point(int x, int y, int z);

    void init(int x, int y, int z);
    void invert(double R, double phi, double theta);
private:
    void changeA4(double A4[4][4],int *point);
    void moveE(int *point, double R, double phi, double theta );
    void multiplication(double A[4][4], double B[4], double RES[4]);
    void turnOnZ(int *point, double phi);
    void turnOnX(int *point, double theta);
    void symmetryXZ(int *point);



};

#endif // POINT_H
