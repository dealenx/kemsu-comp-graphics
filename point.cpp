#include "point.h"

Point::Point()
{

}
Point::Point(int x, int y, int z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}


void Point::setX(int x) {
    this->x = x;
}
void Point::setY(int y) {
    this->y = y;
}
void Point::setZ(int z) {
    this->z = z;
}

void Point::setSX(int sx) {
    this->sx = sx;
}
void Point::setSY(int sy) {
    this->sy = sy;
}
void Point::setSZ(int sz) {
    this->sz = sz;
}

void Point::init(int x, int y, int z) {
    setX(x);
    setY(y);
    setZ(z);

    setSX(x);
    setSY(y);
    setSZ(z);
}

void Point::calcVectorToLight(int normal[3] ,int xLight, int yLight, int zLight) {
    int light[3] = {xLight, yLight, zLight};

    this->vToLight[0] = light[0] - this->x;
    this->vToLight[1] = light[1] - this->y;
    this->vToLight[2] = light[2] - this->z;

    double x1, x2, y1, y2, z1,z2;

    x1 = this->vToLight[0];
    y1 = this->vToLight[1];
    z1 = this->vToLight[2];

    x2 = normal[0];
    y2 = normal[1];
    z2 = normal[2];

    double cosUngle =( x1*x2 + y1*y2 + z1*z2 ) / ( sqrt( x1*x1 + y1*y1 + z1*z1 ) * sqrt( x2*x2 + y2*y2 + z2*z2 )  );

    colorR = (int) (255* cosUngle);
    colorG = (int) (255* cosUngle);
    colorB = (int) (255* cosUngle);

    cout << "color R: " << colorR << endl;


}

void Point::invert(double R, double phi, double theta) {
    int * e = new int[3];
    e[0] = this->x;
    e[1] = this->y;
    e[2] = this->z;
    moveE(e, R, phi, theta);
    turnOnZ( e, phi);
    turnOnX(e, theta);
    symmetryXZ(e);
    this->setSX(e[0]);
    this->setSY(e[1]);
    this->setSZ(e[2]);


    double d = 200;

    double tmpSx = ( (sx * 1.0) / (sz *1.0 ) ) * d;
    double tmpSy = ( (sy* 1.0) /  (sz* 1.0 ) ) * d;

    int tmpIntSx = static_cast< int > (tmpSx);
    int tmpIntSy = static_cast< int > (tmpSy);


    this->setSX( round(tmpIntSx) );
    this->setSY( round(tmpIntSy) );
}

void Point::changeA4(double A4[4][4],int *point) {
    double tmpX = point[0];
    double tmpY = point[1];
    double tmpZ = point[2];
    double V1[4] = {tmpX, tmpY, tmpZ, 1};
    double RES[4];
    multiplication(A4, V1, RES);
    point[0] = RES[0];
    point[1] = RES[1];
    point[2] = RES[2];
}

void Point::multiplication(double A[4][4], double B[4], double RES[4])
{
    for (int i=0;i<4;i++)
    {
        RES[i] = 0;
        for (int j=0;j<4;j++)
        {
            RES[i]+=( A[j][i]*B[j]);
        }
     }
}


void Point::moveE(int *point, double R, double phi, double theta )
{
    double a=R*sin(theta)*cos(phi);
    double b=R*sin(theta)*sin(phi);
    double c=R*cos(theta);
    double A4[4][4]={ {1,0,0,0},{0,1,0,0},{0,0,1,0},{-a,-b,-c,1} };
    changeA4(A4,point);
}
void Point::turnOnZ(int *point, double phi)
{
    double tmp=M_PI/2-phi;
    double A4[4][4]={ {cos(tmp),sin(tmp),0,0},{-sin(tmp),cos(tmp),0,0},{0,0,1,0},{0,0,0,1} };
    changeA4(A4,point);
}

void Point::turnOnX(int *point, double theta)
{
    double tmp=-M_PI+theta;
    double A4[4][4]={ {1,0,0,0},{0,cos(tmp),sin(tmp),0},{0,-sin(tmp),cos(tmp),0},{0,0,0,1} };
    changeA4(A4,point);
}

void Point::symmetryXZ(int *point)
{
    double A4[4][4]={ {-1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1} };
    changeA4(A4,point);
}

