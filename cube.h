#ifndef CUBE_H
#define CUBE_H

#include <QPainter>
#include "methods.h"
#include <iostream>

class Cube
{
public:
    Point *beginPoint;
    Point *points[8];

    vector<vector<Point>> bottomFace;
    vector<vector<Point>> topFace;
    vector<vector<Point>> firstFace;
    vector<vector<Point>> againstfirstFace;
    vector<vector<Point>> secondFace;
    vector<vector<Point>> againstsecondFace;

    int topNormal[3] = {0,0,1};
    int bottomNormal[3] = {0,0,-1};

    int againstfirstNormal[3] = {0,1,0};
    int firstNormal[3] = {0,-1,0};

    int againstsecondNormal[3] = {1,0,0};
    int secondNormal[3] = {-1,0,0};

    Point * lightPoint;


    double R,phi,theta;
    Methods *methods;
    int size;
    Cube();
    Cube(int size);
    ~Cube();
    void initMethod(Methods *methods);
    void updateKoef(double R, double phi, double theta);

    void init(int size);
    void update();
    void repaint();
    void invert();

    void move(int x, int y, int z);

    void initLight(int x, int y, int z);
    void moveLight(int x, int y, int z);
    void invertCalc();
    void initFace();
    void paintAffineFace(vector<vector<Point>> face);
    void paintScreenFace(int pointIndex1, int pointIndex2, int pointIndex3, int pointIndex4);
    void paintFun();
};

#endif // CUBE_H
