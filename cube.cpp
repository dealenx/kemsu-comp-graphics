#include "cube.h"

Cube::Cube()
{
    this->size = 100;
    //init(this->size);
}

Cube::Cube(int size) {
    for(int i = 0; i < 8; i++) {
        this->points[i] = new Point();
    }

    beginPoint = new Point(10,10,10);

    init(size);
}

Cube::~Cube()
{
    delete methods;
    delete []*points;
    delete beginPoint;

    bottomFace.clear();
    topFace.clear();
    firstFace.clear();
    againstfirstFace.clear();
    secondFace.clear();
    againstsecondFace.clear();
}
void Cube::init(int size) {
    this->size = size;

    lightPoint = new Point();
    lightPoint->init(0,200,50);
    update();
}

void Cube::update() {
    for(int i = 0; i < 8; i++) {
        this->points[i] = NULL;
        this->points[i] = new Point();
    }

    int beginX = beginPoint->x;
    int beginY = beginPoint->y;
    int beginZ = beginPoint->z;
    this->points[0]->init(beginX, beginY, beginZ);
    this->points[1]->init(beginX + size, beginY, beginZ);
    this->points[2]->init(beginX + size,  beginY + size, beginZ);
    this->points[3]->init(beginX, beginY + size, beginZ);

    this->points[4]->init(beginX, beginY, beginZ + size);
    this->points[5]->init(beginX + size, beginY, beginZ + size);
    this->points[6]->init(beginX + size, beginY + size, beginZ + size);
    this->points[7]->init(beginX, beginY + size, beginZ + size);

    bottomFace.clear();
    topFace.clear();
    firstFace.clear();
    againstfirstFace.clear();
    secondFace.clear();
    againstsecondFace.clear();

    initFace();
}

void Cube::move(int x, int y, int z) {
    beginPoint->x += x;
    beginPoint->y += y;
    beginPoint->z += z;
    update();

}


void Cube::moveLight(int x, int y, int z) {
    lightPoint->x += x;
    lightPoint->y += y;
    lightPoint->z += z;
    update();

}

void Cube::initMethod(Methods *methods)
{
    this->methods = methods;
}

void Cube::invertCalc() {
    lightPoint->invert(R, phi, theta);
    for(int i = 0; i < 8; i++) {
        this->points[i]->invert(R, phi, theta);
    }

}
void Cube::updateKoef(double R, double phi, double theta)
{
    this->R = R;
    this->phi = phi;
    this->theta = theta;
}

void Cube::paintAffineFace(vector<vector<Point>> face) {
    Point *tmpPoint = NULL;
    for(int i = 0; i < this->size; i++) {
        for(int j = 0; j < this->size; j++) {

            tmpPoint = new Point(face[i][j].x, face[i][j].y, face[i][j].z);
            tmpPoint->invert(R, phi, theta);
            //methods->myPaintPoint(tmpPoint, QColor(0, 0, 0, 255));
            methods->myPaintPoint(tmpPoint, QColor(face[i][j].colorR, face[i][j].colorG, face[i][j].colorB, 255));
        }
    }
}
void Cube::paintScreenFace(int pointIndex1, int pointIndex2, int pointIndex3, int pointIndex4) {
    double beginX1 = this->points[pointIndex1]->sx;
    double endX1 = this->points[pointIndex2]->sx;

    double beginY1 = this->points[pointIndex1]->sy;
    double endY1 = this->points[pointIndex2]->sy;

    double beginX2 = this->points[pointIndex3]->sx;
    double endX2 = this->points[pointIndex4]->sx;

    double beginY2 = this->points[pointIndex3]->sy;
    double endY2 = this->points[pointIndex4]->sy;

    int n = 500;

    double hx1 = (this->points[pointIndex2]->sx - this->points[pointIndex1]->sx) / (n * 1.0);
    double hy1 = (this->points[pointIndex2]->y - this->points[pointIndex1]->sy) / (n * 1.0);

    double hx2 = (this->points[pointIndex4]->x - this->points[pointIndex3]->sx) / (n * 1.0);
    double hy2 = (this->points[pointIndex4]->y - this->points[pointIndex3]->sy) / (n * 1.0);

    for(int i = 0 ; i < n ; i++) {
        beginX1+= hx1;
        beginY1+= hy1;

        beginX2+= hx2;
        beginY2+= hy2;

        methods->MyPaintLine(beginX1,beginY1, beginX2,beginY2);
    }
}
void Cube::initFace() {


    /*
     *
     * Вверхняя грань
     *
     * Иницилизация и заполнение координат
     *
    */

    int indexI = 0;
    int indexJ = 0;

    topFace.resize(size);
    for (int i = 0; i < size; ++i)
        topFace[i].resize(size);

   for(int i = this->points[0]->x; i < this->points[1]->x; i++) {
            for(int j = this->points[0]->y; j < this->points[3]->y; j++) {
                topFace[indexI][indexJ].init(i, j, this->points[4]->z);
                topFace[indexI][indexJ].calcVectorToLight(topNormal ,lightPoint->x, lightPoint->y, lightPoint->z);
                indexJ++;
            }
            indexI++;
            indexJ = 0;
     }
   indexI = indexJ = 0;


    /*
     *
     * Нижняя грань
     *
     * Иницилизация и заполнение координат
     *
    */

    bottomFace.resize(size);
    for (int i = 0; i < size; ++i)
        bottomFace[i].resize(size);

    for(int i = this->points[0]->x; i < this->points[1]->x; i++) {
        for(int j = this->points[0]->y; j < this->points[2]->y; j++) {
            bottomFace[indexI][indexJ].init(i, j, this->points[0]->z);
            bottomFace[indexI][indexJ].calcVectorToLight(bottomNormal ,lightPoint->x, lightPoint->y, lightPoint->z);
            indexJ++;
        }
        indexI++;
        indexJ = 0;
    }
    indexI = indexJ = 0;

    /*
     *
     * Грань 1
     *
     * Иницилизация и заполнение координат
     *
    */

    firstFace.resize(size);
    for (int i = 0; i < size; ++i)
        firstFace[i].resize(size);

    for(int i = this->points[0]->x; i < this->points[1]->x; i++) {
        for(int j = this->points[0]->z; j < this->points[4]->z; j++) {
            firstFace[indexI][indexJ].init(i,this->points[0]->y,j);
            firstFace[indexI][indexJ].calcVectorToLight(firstNormal ,lightPoint->x, lightPoint->y, lightPoint->z);
            indexJ++;
        }
        indexI++;
        indexJ = 0;
    }
    indexI = indexJ = 0;


    /*
     *
     * Напротив 1 Грани
     *
     * Иницилизация и заполнение координат
     *
    */

    againstfirstFace.resize(size);
    for (int i = 0; i < size; ++i)
        againstfirstFace[i].resize(size);
    againstfirstFace[0][0].init(0,0,0);




    for(int i = this->points[3]->x; i < this->points[2]->x; i++) {
        for(int j = this->points[3]->z; j < this->points[7]->z; j++) {
            againstfirstFace[indexI][indexJ].init(i,this->points[3]->y,j);
            againstfirstFace[indexI][indexJ].calcVectorToLight(againstfirstNormal ,lightPoint->x, lightPoint->y, lightPoint->z);
            indexJ++;
        }
        indexI++;
        indexJ = 0;
    }
    indexI = indexJ = 0;

    /*
     *
     * 2 Грань
     *
     * Иницилизация и заполнение координат
     *
    */

    secondFace.resize(size);
    for (int i = 0; i < size; ++i)
        secondFace[i].resize(size);
    secondFace[0][0].init(0,0,0);

    for(int i = this->points[0]->y; i < this->points[3]->y; i++) {
        for(int j = this->points[0]->z; j < this->points[4]->z; j++) {
            secondFace[indexI][indexJ].init(this->points[0]->x,i,j);
            secondFace[indexI][indexJ].calcVectorToLight(secondNormal ,lightPoint->x, lightPoint->y, lightPoint->z);
            indexJ++;
        }
        indexI++;
        indexJ = 0;
    }
    indexI = indexJ = 0;

    /*
     *
     * Напротив 2 Грани
     *
     * Иницилизация и заполнение координат
     *
    */

    againstsecondFace.resize(size);
    for (int i = 0; i < size; ++i)
        againstsecondFace[i].resize(size);
    againstsecondFace[0][0].init(0,0,0);

    for(int i = this->points[1]->y; i < this->points[2]->y; i++) {
        for(int j = this->points[1]->z; j < this->points[5]->z; j++) {
            againstsecondFace[indexI][indexJ].init(this->points[2]->x,i,j);
            againstsecondFace[indexI][indexJ].calcVectorToLight(againstsecondNormal ,lightPoint->x, lightPoint->y, lightPoint->z);
            indexJ++;
        }
        indexI++;
        indexJ = 0;
    }
    indexI = indexJ = 0;
}
void Cube::repaint() {
        //paintFun();
        methods->myPaintPoint(lightPoint, QColor(255,255,255), 2.5);

        paintAffineFace(bottomFace);

        paintAffineFace(secondFace);


        paintAffineFace(againstsecondFace);
        paintAffineFace(firstFace);
        paintAffineFace(topFace);
        paintAffineFace(againstfirstFace);
/*
        methods->MyPaintLine(this->points[0], this->points[1]);
        methods->MyPaintLine(this->points[0], this->points[3]);
        methods->MyPaintLine(this->points[1], this->points[2]);
        methods->MyPaintLine(this->points[3], this->points[2]);

        methods->MyPaintLine(this->points[4], this->points[5]);
        methods->MyPaintLine(this->points[4], this->points[7]);
        methods->MyPaintLine(this->points[5], this->points[6]);
        methods->MyPaintLine(this->points[7], this->points[6]);

        methods->MyPaintLine(this->points[0], this->points[4]);
        methods->MyPaintLine(this->points[1], this->points[5]);
        methods->MyPaintLine(this->points[2], this->points[6]);
        methods->MyPaintLine(this->points[3], this->points[7]);*/
}
void Cube::paintFun() {
    /*
     *
     * Прикол))0)
     *
    */
    Point *tmpPoint = new Point(50, 50, 0);
            for(int i = this->points[0]->x; i < this->points[1]->x; i++) {
                for(int j = this->points[0]->y; j < this->points[2]->y; j++) {
                    tmpPoint->invert(R, phi, theta);
                    methods->myPaintPoint(tmpPoint);
               }
            }
}
