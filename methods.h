#ifndef METHODS_H
#define METHODS_H

#include <iostream>
#include <QPainter>
#include "math.h"
#include "point.h"

using namespace std;

class Methods
{
private:
    QPainter *painter;

    int widthWindow, heightWindow;
public:
    void initQPainter(QPainter *painter, int widthWindow, int heightWindow);

    void MyPaintLine(int x1, int y1, int x2, int y2);
    void MyPaintLine(Point *point1, Point *point2);
    void myPaintCircle(double x0, double y0, int radius);
    void myPaintCircle (Point *point, int radius);
    void myPaintPoint(int x, int y);
    void myPaintPoint(Point *point);
    void myPaintPoint(Point *point, QColor color);
    void myPaintPoint(Point *point, QColor color, double size);

    Methods();
};

#endif // METHODS_H
