#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    init();
    invertCalc();

}
void MainWindow::init()
{
    methods = new Methods();

    setCoordinate();

    e0Point = new Point();
    exPoint = new Point();
    eyPoint = new Point();
    ezPoint = new Point();

    cube = new Cube(20);

    resetObjects();

    widthWindow = width() - 251;
    heightWindow = height();
}

void MainWindow::resetObjects() {
    e0Point->init(0, 0, 0);
    exPoint->init(200, 0, 0);
    eyPoint->init(0, 200, 0);
    ezPoint->init(0, 0, 200);
}

void MainWindow::setCoordinate()
{
    R = 300;
    theta = M_PI/4.0;
    phi = M_PI/4.0;
}

void MainWindow::invertCalc() {
    e0Point->invert(R, phi, theta);
    exPoint->invert(R, phi, theta);
    eyPoint->invert(R, phi, theta);
    ezPoint->invert(R, phi, theta);

    cube->updateKoef(R, phi, theta);
    cube->invertCalc();
}

void MainWindow::paintEvent(QPaintEvent *event) {
    painter = new QPainter(this);
    methods->initQPainter(painter, widthWindow, heightWindow);
    cube->initMethod(methods);

    methods->MyPaintLine(e0Point, exPoint);
    methods->MyPaintLine(e0Point, eyPoint);
    methods->MyPaintLine(e0Point, ezPoint);

    methods->myPaintCircle (exPoint, 3);
    methods->myPaintCircle (eyPoint, 6);

    cube->repaint();

}

MainWindow::~MainWindow()
{
    delete methods;
    delete e0Point;
    delete exPoint;
    delete eyPoint;
    delete ezPoint;
    delete cube;

    delete painter;
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    phi+=(M_PI/180)*20;
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_3_clicked()
{
    phi-=(M_PI/180)*20;
    invertCalc();
    repaint();
}


void MainWindow::on_pushButton_4_clicked()
{
    theta-=(M_PI/180)*20;
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_5_clicked()
{
    theta+=(M_PI/180)*20;
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_2_clicked()
{
    cube->move(-30, 0, 0);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_6_clicked()
{
    cube->move(+30, 0, 0);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_7_clicked()
{
    cube->move(0, -30, 0);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_8_clicked()
{
    cube->move(0, +30, 0);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_9_clicked()
{
    cube->move(0, 0, -30);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_10_clicked()
{
    cube->move(0, 0, +30);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_11_clicked()
{
    int size = cube->size + 20;

    if(size  > 141) {
        size = 140;
    }
    cube->init(size);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_12_clicked()
{
    int size = cube->size - 20;
    if(size  < 9) {
        size = 10;
    }
    cube->init(size);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_13_clicked()
{
    cube->moveLight(-30, 0, 0);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_14_clicked()
{
    cube->moveLight(+30, 0, 0);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_15_clicked()
{
    cube->moveLight(0, -30, 0);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_16_clicked()
{
    cube->moveLight(0, +30, 0);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_17_clicked()
{
    cube->moveLight(0, 0, +30);
    invertCalc();
    repaint();
}

void MainWindow::on_pushButton_18_clicked()
{
    cube->moveLight(0, 0, -30);
    invertCalc();
    repaint();
}
